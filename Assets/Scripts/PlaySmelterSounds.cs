﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySmelterSounds : MonoBehaviour
{
    public AudioSource meltingAudio;
    public AudioSource pouringAudio;
    public AudioSource purgingAudio;


    public void PlayPourSound(){
        pouringAudio.Play();
    }

    public void PlayMeltingSound(){
        meltingAudio.Play();
    }

    public void PlayPurgingSound(){
        purgingAudio.Play();
    }
}
