﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalContainer : Interactable {

    public MetalType metal = MetalType.Copper;
    

    public GameObject metalIngotPrefab;

    // PlayerInteraction playerInteraction; SET IN PARENT

    protected override void Awake(){
        base.Awake();
        objectName = metal.ToString();
    }
    
    public override void Activate(){ 
        if(playerInteraction.objectHeld == null){
            playerInteraction.GivePlayerItem(SpawnMetal());
        }
    }

    GameObject SpawnMetal(){
        GameObject metalSpawned =  Instantiate(metalIngotPrefab);
        metalSpawned.GetComponent<Metal>().metalType = metal;
        metalSpawned.GetComponent<Interactable>().objectName = metal.ToString();
        return metalSpawned;
    }
}
