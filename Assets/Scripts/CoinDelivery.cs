﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using TMPro;

public class CoinDelivery : MonoBehaviour {

    public int nrOfFailuresLeft = 1;

    public GameObject dieCanvas;
    public TextMeshPro text;

    public AudioSource positiveAudio;
    public AudioSource negativeAudio;
    public Light errorLight;


    float errorLightAnimationTime = 2f;
    

    void JudgeCoin(Coin coinData){
        if(IsCoinCorrect(General.Instance.currentCoin, coinData)){
            Debug.Log("WE MADE THE CORRECT COIN!?");
            positiveAudio.Play();
            General.Instance.PickRandomCoin();
        }else{
            negativeAudio.Play();
            StartCoroutine(ErrorLightAnimation());
            nrOfFailuresLeft -= 1;
            text.SetText("Faults permitted: " + nrOfFailuresLeft.ToString());
            if(nrOfFailuresLeft < 0){
                StartCoroutine(DieAnimation());
            }
            Debug.Log("WRONG!");
        }
    }

    IEnumerator ErrorLightAnimation(){
        float elapsedTime = 0f;
        while( elapsedTime < errorLightAnimationTime){
            errorLight.intensity = Mathf.PingPong(elapsedTime*10f, 10f);
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator DieAnimation(){
        dieCanvas.SetActive(true);
        yield return new WaitForSeconds(2f);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        SceneManager.LoadScene("GameOver");
    }

    private bool IsCoinCorrect(Coin referenceCoin, Coin madeCoin){ 

    return  (Enumerable.SequenceEqual(referenceCoin.metals.OrderBy(t => t), madeCoin.metals.OrderBy(t => t)) && 
            Enumerable.SequenceEqual(referenceCoin.alloys.OrderBy(t => t), madeCoin.alloys.OrderBy(t => t)) &&
            referenceCoin.size == madeCoin.size &&
            referenceCoin.coinCast == madeCoin.coinCast && referenceCoin.coinCast == madeCoin.coinCast2);
    }

    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<CoinObject>()){
            JudgeCoin(other.GetComponent<CoinObject>().coinData);
            Destroy(other.gameObject);
        }
    }
}
