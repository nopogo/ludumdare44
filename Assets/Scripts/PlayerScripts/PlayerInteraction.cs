﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour {

    public float maxInteractionDistance = 2f;
    public Transform holdObjectParent;
    Interactable objectLookedAt;
    

    public Interactable objectHeld = null;

    void Update(){
       InteractWithObject();
    }

    public void GivePlayerItem(GameObject prefab){
        prefab.GetComponent<Interactable>().Pickup();
        objectHeld = prefab.GetComponent<Interactable>();
    }

    void InteractWithObject(){
        objectLookedAt = GetInteractableObject();

        if(Input.GetButtonUp("Fire1")){
            if(objectHeld == null && objectLookedAt != null){
                Debug.Log("picking up object");
                if(objectLookedAt.isPhysicsObject){
                   
                    objectLookedAt.Pickup();
                    objectHeld = objectLookedAt;
                } else{
                    Debug.Log("activating object! ");
                    objectLookedAt.Activate();
                }
                
            }  else if(objectHeld != null){
                Debug.Log("dropping object");
                objectHeld.Drop();
                objectHeld = null;
            }        
        }
       
    }

    Interactable GetInteractableObject(){
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, maxInteractionDistance)){
            if(hit.transform.GetComponent<Interactable>()!= null){
                return hit.transform.GetComponent<Interactable>();
            }
        }
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * maxInteractionDistance, Color.red);
        return null;
    }
}
