﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;


public enum MetalType {
    Default,
    Copper,
    Nickel,
    Zinc,
    Platinum,
    Gold,
    Aluminium,
    Silver,
    Tin,
    Iron,
    Chromium
}

public enum AlloyType{
    Bronze,
    Brass,
    StainlessSteel,
    SterlingSilver
}

public class Alloy{
    public AlloyType alloyType;
    public MetalType metalType1;
    public MetalType metalType2;
}

public class General : MonoBehaviour{

    public List<Coin> coins;
    public Alloy[] alloys = new Alloy[4];
    public Coin currentCoin;
    public TextMeshPro textMeshPro;

    private static General _instance;
    public static General Instance { get { return _instance; }}
    public GameObject prefabIngot;

    void Awake(){

        if (_instance != null && _instance != this) {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }

        InitiateAlloys();
        CreateCoins();
        PickRandomCoin();
    }

    public void PickRandomCoin(){
        currentCoin = coins[Random.Range(0, coins.Count)];
        textMeshPro.SetText(currentCoin.name);
    }


     void InitiateAlloys(){
        Alloy tempAlloy = new Alloy();
        tempAlloy.alloyType  = AlloyType.Bronze;
        tempAlloy.metalType1 = MetalType.Copper;
        tempAlloy.metalType2 = MetalType.Tin;
        alloys[0] = tempAlloy;

        Alloy tempAlloy2 = new Alloy();
        tempAlloy2.alloyType  = AlloyType.Brass;
        tempAlloy2.metalType1 = MetalType.Copper;
        tempAlloy2.metalType2 = MetalType.Zinc;
        alloys[1] = tempAlloy2;

        Alloy tempAlloy3 = new Alloy();
        tempAlloy3.alloyType  = AlloyType.StainlessSteel;
        tempAlloy3.metalType1 = MetalType.Iron;
        tempAlloy3.metalType2 = MetalType.Chromium;
        alloys[2] = tempAlloy3;

        Alloy tempAlloy4 = new Alloy();
        tempAlloy4.alloyType  = AlloyType.SterlingSilver;
        tempAlloy4.metalType1 = MetalType.Copper;
        tempAlloy4.metalType2 = MetalType.Silver;
        alloys[3] = tempAlloy4;
    }
    void CreateCoins(){

        coins = new List<Coin>();

        Coin tempCoin = new Coin();
        MetalType[] tempListMetals = new MetalType[] {};
        AlloyType[] tempAlloyList  = new AlloyType[] {AlloyType.Bronze, AlloyType.StainlessSteel};
        tempCoin.name      = "British penny";
        tempCoin.metals    = tempListMetals;
        tempCoin.alloys    = tempAlloyList;
        tempCoin.size      = CoinSize.Medium;
        tempCoin.coinCast  = CoinCast.British;
        tempCoin.coinCast2 = CoinCast.British;
        coins.Add(tempCoin);

        Coin tempCoin2 = new Coin();
        MetalType[] tempListMetals1 = new MetalType[] {MetalType.Copper, MetalType.Nickel};
        AlloyType[] tempAlloyList1  = new AlloyType[] {AlloyType.StainlessSteel};
        tempCoin2.name      = "Canadian nickel";
        tempCoin2.metals    = tempListMetals1;
        tempCoin2.alloys    = tempAlloyList1;
        tempCoin2.size      = CoinSize.Medium;
        tempCoin2.coinCast  = CoinCast.Canadian;
        tempCoin2.coinCast2 = CoinCast.Canadian;
        coins.Add(tempCoin2);

        Coin tempCoin3 = new Coin();
        MetalType[] tempListMetals2 = new MetalType[] {};
        AlloyType[] tempAlloyList2  = {AlloyType.SterlingSilver};
        tempCoin3.name      = "Norman Silver penny";
        tempCoin3.metals    = tempListMetals2;
        tempCoin3.alloys    = tempAlloyList2;
        tempCoin3.size      = CoinSize.Medium;
        tempCoin3.coinCast  = CoinCast.Norman;
        tempCoin3.coinCast2 = CoinCast.Norman;
        coins.Add(tempCoin3);

        Coin tempCoin4 = new Coin();
        MetalType[] tempListMetals3 = new MetalType[] {MetalType.Copper, MetalType.Zinc};
        AlloyType[] tempAlloyList3  = new AlloyType[] {};
        tempCoin4.name      = "United States penny";
        tempCoin4.metals    = tempListMetals3;
        tempCoin4.alloys    = tempAlloyList3;
        tempCoin4.size      = CoinSize.Medium;
        tempCoin4.coinCast  = CoinCast.US;
        tempCoin4.coinCast2 = CoinCast.US;
        coins.Add(tempCoin4);

        Coin tempCoin5 = new Coin();
        MetalType[] tempListMetals4 = new MetalType[] {MetalType.Gold};
        AlloyType[] tempAlloyList4 = new AlloyType[] {};
        tempCoin5.name      = "Persian daric";
        tempCoin5.metals    = tempListMetals4;
        tempCoin5.alloys    = tempAlloyList4;
        tempCoin5.size      = CoinSize.Medium;
        tempCoin5.coinCast  = CoinCast.PersianDaric;
        tempCoin5.coinCast2 = CoinCast.PersianDaric;
        coins.Add(tempCoin5);

        Coin tempCoin6 = new Coin();
        MetalType[] tempListMetals5 = new MetalType[] {MetalType.Silver};
        AlloyType[] tempAlloyList5  = new AlloyType[] {};
        tempCoin6.name      = "Persian siglos";
        tempCoin6.metals    = tempListMetals5;
        tempCoin6.alloys    = tempAlloyList5;
        tempCoin6.size      = CoinSize.Small;
        tempCoin6.coinCast  = CoinCast.PersianSiglos;
        tempCoin6.coinCast2 = CoinCast.PersianSiglos;
        coins.Add(tempCoin6);

        Coin tempCoin7 = new Coin();
        MetalType[] tempListMetals6 = new MetalType[] {MetalType.Platinum};
        AlloyType[] tempAlloyList6  = new AlloyType[] {};
        tempCoin7.name      = "Australian koala";
        tempCoin7.metals    = tempListMetals6;
        tempCoin7.alloys    = tempAlloyList6;
        tempCoin7.size      = CoinSize.Large;
        tempCoin7.coinCast  = CoinCast.Australian;
        tempCoin7.coinCast2 = CoinCast.Australian;
        coins.Add(tempCoin7);

        Coin tempCoin8 = new Coin();
        MetalType[] tempListMetals7 = new MetalType[] {MetalType.Aluminium};
        AlloyType[] tempAlloyList7  = new AlloyType[] {};
        tempCoin8.name      = "Japanese yen";
        tempCoin8.metals    = tempListMetals7;
        tempCoin8.alloys    = tempAlloyList7;
        tempCoin8.size      = CoinSize.Small;
        tempCoin8.coinCast  = CoinCast.Japanese;
        tempCoin8.coinCast2 = CoinCast.Japanese;
        coins.Add(tempCoin8);

        Coin tempCoin9 = new Coin();
        MetalType[] tempListMetals8 = new MetalType[] {};
        AlloyType[] tempAlloyList8  = new AlloyType[] {AlloyType.Brass};
        tempCoin9.name      = "Chinese kiangnan";
        tempCoin9.metals    = tempListMetals8;
        tempCoin9.alloys    = tempAlloyList8;
        tempCoin9.size      = CoinSize.Medium;
        tempCoin9.coinCast  = CoinCast.Chinese;
        tempCoin9.coinCast2 = CoinCast.Chinese;
        coins.Add(tempCoin9);
    }

    public void SpawnIngot(){
        Forge forge = GameObject.FindObjectOfType<Forge>();
        if(forge.metalContent.Count > 0 || forge.alloyContent.Count > 0){
            GameObject ingotGameObject = Instantiate(prefabIngot, forge.spawnLocation.position, Quaternion.identity);
            forge.CastMetal(ingotGameObject.GetComponent<CastMetal>());
        }
    }
}
