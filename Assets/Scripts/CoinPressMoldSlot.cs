﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(BoxCollider))]
public class CoinPressMoldSlot : MonoBehaviour{
    
    // ColdPress coldPress;
    public CoinPressMold slottedCoinPressMold;

    public Transform orientationTransform;

    // void Awake(){
    //     coldPress = GameObject.FindObjectOfType<ColdPress>();
    // }

    public void ResetMoldSlot(){
        slottedCoinPressMold = null;
    }

    private void OnTriggerEnter(Collider other) {
        Debug.Log("It triggers");
        if(other.GetComponent<CoinPressMold>()){
            slottedCoinPressMold = other.GetComponent<CoinPressMold>();
            other.GetComponent<AudioSource>().Play();
            other.GetComponent<Rigidbody>().useGravity  = false;
            other.GetComponent<Rigidbody>().isKinematic = true;
            other.transform.position = orientationTransform.position;
            other.transform.rotation = orientationTransform.rotation;
            other.transform.SetParent(orientationTransform);
            other.transform.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
}
