﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CoinSize {
   Small,
   Medium,
   Large
}

public enum CoinCast {
   British,
   Canadian,
   Norman,
   US,
   PersianDaric,
   PersianSiglos,
   Australian,
   Japanese,
   Chinese
}

public class Coin {
    public string name = "default";
    public MetalType[] metals = new MetalType[0];
    public AlloyType[] alloys = new AlloyType[0];
    public CoinSize size = CoinSize.Small;
    public CoinCast coinCast = CoinCast.British;
    public CoinCast coinCast2 = CoinCast.British;
}
