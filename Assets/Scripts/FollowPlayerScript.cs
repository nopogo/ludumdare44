﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class FollowPlayerScript : MonoBehaviour {

    Transform player;
    float damping = 1f;

    void Awake(){
        player = GameObject.FindObjectOfType<FirstPersonController>().transform;
    }
    void Update(){
        FollowPlayer();
    }

    void FollowPlayer(){
        var lookPos = player.position - transform.position;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        rotation *= Quaternion.Euler(0f,180f, 0f);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
    }
}
