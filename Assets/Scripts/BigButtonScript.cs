﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ButtonActions{
    Purge,
    Run,
    Empty,
    Small,
    Medium,
    Large
}

[RequireComponent(typeof(Outline))]
public class BigButtonScript : MonoBehaviour {

    public WailaScript wailaScript;

    public bool isPressed = false;

    public ButtonActions buttonAction = ButtonActions.Run;
    public Machine linkedMachine;

    void Awake(){
        wailaScript = (WailaScript) GameObject.FindObjectOfType<WailaScript>();
    }
    
    // protected override void Awake(){
    //     base.Awake();
    //     // GetComponent<Interactable>().objectName = "Cold Press";
    // }

   private void OnMouseDown() {
      
       linkedMachine.ButtonPress(buttonAction);
   }

   void OnMouseOver(){
        GetComponent<Outline>().OutlineMode = Mode.OutlineAll;
        wailaScript.SetText(buttonAction.ToString());
    }

    void OnMouseExit(){
        GetComponent<Outline>().OutlineMode = Mode.Disabled;
        wailaScript.HideText();
    }
}
