﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CastMetal : MonoBehaviour {

    public List<MetalType> metals = new List<MetalType>();
    public List<AlloyType> alloys = new List<AlloyType>();


    public void Define(ArrayList metalTypes, ArrayList alloyTypes){
        foreach(MetalType metal in metalTypes){
            metals.Add(metal);
            Debug.Log(metal.ToString());
        }

        foreach(AlloyType alloy in alloyTypes){
            alloys.Add(alloy);
            Debug.Log(alloy.ToString());
        }

    }
}
