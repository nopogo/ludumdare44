﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ColdPress : Machine {

    public BigButtonScript smallButton;
    public BigButtonScript mediumButton;
    public BigButtonScript largeButton;

    public CoinPressMoldSlot coinPressSlot;
    public CoinPressMoldSlot coinPressSlot2;

    public Transform[] irisGameObjects;

    public List<MetalType> dumpedMetals;
    public List<AlloyType> dumpedAlloys;

    public Animator coldPressAnimator;
    public GameObject prefabCoinObject;
    public Transform coinSpawnPosition;

    public AudioSource coldPressAudioSource;
    public AudioSource buttonPressAudio;
    public AudioSource irisAudio;

    CoinSize buttonCurrentlyPressed = CoinSize.Large;
    
    
    public void ChangeIrisRotation(float yFloat){
        if(irisAudio != null){
            irisAudio.Play();
        }
        foreach(Transform trans in irisGameObjects ){
            trans.rotation = Quaternion.Euler(0f, yFloat+90, 0f);
        }
    }
    public override void ButtonPress(ButtonActions action){
        // if(anim.GetCurrentAnimatorStateInfo(0).IsName("Standby")){
        switch(action){
            case ButtonActions.Small:
                // ChangeIrisRotation(-23f);
                coldPressAnimator.SetTrigger("Small");
                smallButton.isPressed = true;
                mediumButton.isPressed = false;
                largeButton.isPressed = false;
                buttonCurrentlyPressed = CoinSize.Small;

                break;
            case ButtonActions.Medium:
                // ChangeIrisRotation(-11.5f);
                coldPressAnimator.SetTrigger("Medium");
                smallButton.isPressed = false;
                mediumButton.isPressed = true;
                largeButton.isPressed = false;
                buttonCurrentlyPressed = CoinSize.Medium;
                break;
            case ButtonActions.Large:
                // ChangeIrisRotation(0f);
                coldPressAnimator.SetTrigger("Large");
                smallButton.isPressed = false;
                mediumButton.isPressed = false;
                largeButton.isPressed = true;
                buttonCurrentlyPressed = CoinSize.Large;
                break;
            case ButtonActions.Run:
                if(coinPressSlot.slottedCoinPressMold != null && coinPressSlot2.slottedCoinPressMold != null){
                    if(dumpedMetals.Count > 0 || dumpedAlloys.Count > 0){
                        if(coldPressAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pressing") == false){
                            coldPressAnimator.SetTrigger("Power");
                        }
                    }
                }else{
                    Debug.LogError("Missing materials, error sound?");
                }
                break;
        }        
    }

    public void PlayPressSound(){
        coldPressAudioSource.Play();
    }

    public void PlayButtonAudio(){
        if(buttonPressAudio != null){
           buttonPressAudio.Play();
       }
   }

    public void SpawnCoin(){
        if(dumpedMetals == null || dumpedAlloys == null || coinPressSlot.slottedCoinPressMold == null || coinPressSlot2.slottedCoinPressMold == null){
            Debug.LogError("NO INPUT METAL, WHY ARE WE HERE?");
            return;
        }
        Coin punchedCoin      = new Coin();
        punchedCoin.alloys    = dumpedAlloys.ToArray();
        punchedCoin.metals    = dumpedMetals.ToArray();
        punchedCoin.size      = buttonCurrentlyPressed;
        punchedCoin.coinCast  = coinPressSlot.slottedCoinPressMold.coinCast;
        punchedCoin.coinCast2 = coinPressSlot2.slottedCoinPressMold.coinCast;

        GameObject tempCoin = Instantiate(prefabCoinObject, coinSpawnPosition.position, Quaternion.identity);
        Debug.Log("SpawnCoin");
        tempCoin.GetComponent<CoinObject>().coinData = punchedCoin;
    }

   

    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<CastMetal>()){
            dumpedMetals = other.GetComponent<CastMetal>().metals;
            dumpedAlloys = other.GetComponent<CastMetal>().alloys;
            if(GetComponent<AudioSource>()){
                GetComponent<AudioSource>().Play();
            }           
            Destroy(other.gameObject);
        }
    }

    
}
