﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forge : Machine {

    public ArrayList metalContent = new ArrayList();
    public ArrayList alloyContent = new ArrayList();

    public Animator smelterAnim;
    public Animator leverAnim;

    public bool contentIsMelted = false;
    public GameObject prefabMetalOre;
    public Transform spawnLocation;
    public Transform solidSpawnLocation;
    public Transform purgeSpawnLocation;

    public AudioSource rockSound;


    protected override void Awake(){
        base.Awake();
    }

    void AddMetal(MetalType metalType){
        metalContent.Add(metalType);
    }

    void RemoveMetal(MetalType metalType){
        metalContent.Remove(metalType);
    }

    void AddAlloy(AlloyType alloyType){
        alloyContent.Add(alloyType);
    }

    void MeltAction(){
        contentIsMelted = true;
        ArrayList tempMetalContent = metalContent;
        foreach(MetalType metalType in tempMetalContent.ToArray()){
            CalculateAlloy(metalType);
        }
    }

    bool CalculateAlloy(MetalType metalType){
        ArrayList tempMetalContent = metalContent;
        foreach(MetalType metalType2 in tempMetalContent.ToArray()){
            foreach(Alloy alloy in General.Instance.alloys){
                if(metalType == alloy.metalType1 && metalType2 == alloy.metalType2){
                    AddAlloy(alloy.alloyType);
                    RemoveMetal(metalType);
                    RemoveMetal(metalType2);
                    return true;
                }
                if(metalType == alloy.metalType2 && metalType2 == alloy.metalType1){
                    AddAlloy(alloy.alloyType);
                    RemoveMetal(metalType);
                    RemoveMetal(metalType2);
                    return true;
                }
            }
        }
        return false;
    }

    public void CastMetal(CastMetal castMetal){
        castMetal.Define(metalContent, alloyContent);
        metalContent = new ArrayList();
        alloyContent = new ArrayList();
        contentIsMelted = false;
    }

    public void SpawnSolids(bool areWePurging = false){
        foreach(MetalType metalType in metalContent){
            if(areWePurging){
                GameObject tempOreObject = Instantiate(prefabMetalOre, purgeSpawnLocation.position, Quaternion.identity);
                tempOreObject.GetComponent<BoxCollider>().isTrigger = true;
            } else{
                GameObject tempOreObject = Instantiate(prefabMetalOre, solidSpawnLocation.position, Quaternion.identity);
                tempOreObject.GetComponent<Metal>().metalType = metalType;
                tempOreObject.GetComponent<Interactable>().objectName = metalType.ToString();
            }
        }
        metalContent = new ArrayList();
        alloyContent = new ArrayList();
    }

    public override void ButtonPress(ButtonActions action){
        // float distance = Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, transform.position);
        // float maxDistance = 7f; // distance <= maxDistance && 
        if(smelterAnim.GetCurrentAnimatorStateInfo(0).IsName("Standby")){
            
            switch(action){                
                case ButtonActions.Empty:
                    smelterAnim.SetTrigger("Pour");
                    leverAnim.SetTrigger("Tip");
                    Debug.Log("emptying the bucket with!");
                    foreach(MetalType metalType in metalContent){
                        Debug.Log(metalType);
                    }
                    foreach(AlloyType allyType in alloyContent){
                        Debug.Log(allyType);
                    }
                    break;
                case ButtonActions.Purge:
                    smelterAnim.SetTrigger("Dump");
                    leverAnim.SetTrigger("Purge");
                    break;
                case ButtonActions.Run:
                    smelterAnim.SetTrigger("Melting");
                    leverAnim.SetTrigger("Melt");
                    MeltAction();
                    break;
            }
        }
        
    }

    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<Metal>()){
            AddMetal(other.GetComponent<Metal>().metalType);
            rockSound.Play();
            Destroy(other.gameObject);
        }
    }
}
