﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Machine : MonoBehaviour {

    public virtual void ButtonPress(ButtonActions action){

    }

    public WailaScript wailaScript;

    protected virtual void Awake(){
        wailaScript = (WailaScript) GameObject.FindObjectOfType<WailaScript>();
    }

}
