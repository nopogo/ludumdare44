﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof (Rigidbody))]
[RequireComponent(typeof(Outline))]
public class Interactable : MonoBehaviour{


    GameObject player;
    public string objectName = "defaultname";

    public PlayerInteraction playerInteraction;
    public bool isBehingHeld = false;
    public bool isPhysicsObject = false;
    
    public WailaScript wailaScript;

    protected virtual void Awake(){
        playerInteraction = (PlayerInteraction) GameObject.FindObjectOfType<PlayerInteraction>();
        wailaScript = (WailaScript) GameObject.FindObjectOfType<WailaScript>();
    }

    void OnMouseOver(){
        if(isBehingHeld == false){
            GetComponent<Outline>().OutlineMode = Mode.OutlineAll;
            wailaScript.SetText(objectName);

        }else{
            GetComponent<Outline>().OutlineMode = Mode.Disabled;
            wailaScript.HideText();
        }
    }

    void OnMouseExit(){
        GetComponent<Outline>().OutlineMode = Mode.Disabled;
         wailaScript.HideText();
    }

    public virtual void Activate(){

    }



    public void Pickup(){
        if(isBehingHeld){
            Debug.LogError("YOU CANT PICK US UP WE ARE ALREADY BEING HELD!");
        }

        if(GetComponent<AudioSource>()){
            GetComponent<AudioSource>().Play();
        }
        Debug.Log("we attempt a pickup");
        transform.SetParent(playerInteraction.holdObjectParent);
        transform.localPosition = Vector3.zero;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<BoxCollider>().enabled   = false;
        if(GetComponent<CoinPressMold>()){
            GetComponent<CoinPressMold>().TriggerPickup();
        }
        isBehingHeld = true;
    }

    public void Drop(){
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().useGravity  = true;
        GetComponent<BoxCollider>().enabled   = true;
        transform.SetParent(null);
        // transform.localScale = Vector3.one;
        isBehingHeld = false;
    }

    void OnCollisionEnter(Collision hit) {
        if(GetComponent<AudioSource>() && Time.time > 5f){
            GetComponent<AudioSource>().Play();
        }
    }
}
