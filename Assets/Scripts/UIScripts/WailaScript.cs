﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WailaScript : MonoBehaviour{
    
    public Text text;
    public GameObject wailaPanel;

    void Awake(){
        wailaPanel.SetActive(false);
    }

    public void SetText(string textToSet){
        wailaPanel.SetActive(true);
        text.text = textToSet;
    }

    public void HideText(){
        wailaPanel.SetActive(false);
    }
}