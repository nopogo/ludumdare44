﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveTabToFront : MonoBehaviour
{
    public void SelectThisSiblingAsFront(){
        transform.SetAsLastSibling();
    }
}
