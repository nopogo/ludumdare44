﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Manual : Interactable {

    public GameObject manualUI;

    public bool isManualOpen = false;
    FirstPersonController firstPersonController;
    AudioSource bookAudioSource;


    protected override void Awake() {
        base.Awake();
        bookAudioSource = GetComponent<AudioSource>();
        firstPersonController = GameObject.FindObjectOfType<FirstPersonController>();
        GetComponent<Interactable>().objectName = "Manual";
    }

    public void CloseManual(){
        isManualOpen = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;        
        manualUI.SetActive(false);
        firstPersonController.enabled = true;
    }

    void Update() {
        if(isManualOpen){
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetKeyUp(KeyCode.Escape) && isManualOpen){
            CloseManual();
        }
    }

    public override void Activate() {
        if (!isManualOpen) {
            bookAudioSource.Play();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            isManualOpen = !isManualOpen;
            manualUI.SetActive(isManualOpen);
            firstPersonController.enabled = !isManualOpen;
        }

    }


}







