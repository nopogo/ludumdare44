﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class MenuScript : Singleton<MenuScript> {

    public GameObject menuPanel;
    public GameObject startGameButton;

    bool isActive = true;

    FirstPersonController firstPersonController;

    public override void Awake(){
        if(SceneManager.GetActiveScene().name == "MainScene"){
           firstPersonController  = GameObject.FindObjectOfType<FirstPersonController>();
        }
        base.Awake();
    }

    void Update(){
        if(SceneManager.GetActiveScene().name == "GameOver"){
            Destroy(gameObject);
        }
        if(SceneManager.GetActiveScene().name != "MainMenu"){
            Manual manual = GameObject.FindObjectOfType<Manual>();
            if (manual != null && manual.isManualOpen == false){
                if(Input.GetKeyUp(KeyCode.Escape)){
                    isActive = !isActive;
                }
            }
        }
    

        if(SceneManager.GetActiveScene().name == "MainScene"){
            startGameButton.SetActive(false);
        }else{
            startGameButton.SetActive(true);
        }
        menuPanel.SetActive(isActive);

        if(isActive){
            if(firstPersonController != null){
                firstPersonController.enabled = false;
            }
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }else{
             if(firstPersonController != null){
                firstPersonController.enabled = true;
            }
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    public void StartGame(){
        isActive = false;
        //SceneManager.GetSceneByName("MainScene").buildIndex
        StartCoroutine(LoadYourAsyncScene("MainScene"));
    }

    public void ExitGame(){
        Application.Quit();
    }

    IEnumerator LoadYourAsyncScene(string sceneName) {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone) {
            yield return null;
        }
    }
}
