﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverSound : MonoBehaviour {
   public void PlayLeverAudio(){
        GetComponent<AudioSource>().Play();
    }
}
