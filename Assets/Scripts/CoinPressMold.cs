﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPressMold : Interactable {

    CoinPressMoldSlot currentCoinPressSlot = null;
    public CoinCast coinCast = CoinCast.British;

    // Transform parentTransform;


    protected override void Awake(){
        base.Awake();
        // parentTransform = transform.parent;
        GetComponent<Interactable>().objectName = coinCast.ToString();
    }

    public void TriggerDrop(CoinPressMoldSlot slot){
        currentCoinPressSlot = slot;
        transform.SetParent(null);
    }
    public void TriggerPickup(){
        if(currentCoinPressSlot != null){
            transform.SetParent(GameObject.FindGameObjectWithTag("HoldPosition").transform);
            
            currentCoinPressSlot.ResetMoldSlot();
            currentCoinPressSlot = null;
        }
        
    }
}
