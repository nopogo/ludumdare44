﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateForge : MonoBehaviour{

    public ParticleSystem meltedParticles;
    public ParticleSystem smokeParticles;
    Forge forge;

    void Awake(){
        forge = GameObject.FindObjectOfType<Forge>();
    }


    public void StartSmoke(){
        smokeParticles.Play();
    }

    public void StopSmoke(){
        smokeParticles.Stop();
    }

    public void StartAnimatingParticles (){
        if(forge.metalContent.Count > 0 || forge.alloyContent.Count > 0){
            if(forge.contentIsMelted){
                meltedParticles.Play();
            }else{
                forge.SpawnSolids();
            }            
        }
    }
    public void StartDumpingParticles(){
        Debug.Log("the squirls have been silent");
        if(forge.metalContent.Count > 0 || forge.alloyContent.Count > 0){
            Debug.Log("the squirls have been silent AwfwwfawfawfawfNO MO");
            if(forge.contentIsMelted){
                Debug.Log("the squirls have been silent NO MO");
                meltedParticles.Play();
            }else{
                Debug.Log("the squirls have been silent NO MO SOLID");
                forge.SpawnSolids(true);
            }            
        }
        forge.contentIsMelted = false;
        forge.metalContent = new ArrayList();
        forge.alloyContent = new ArrayList();
    }

    public void SpawnIngot(){
        if(forge.contentIsMelted){
            General.Instance.SpawnIngot();
        }
    }

    public void StopAnimatingParticles(){
        meltedParticles.Stop();
    }
}
