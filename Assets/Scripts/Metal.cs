﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Metal : MonoBehaviour{
    public MetalType metalType = MetalType.Default;
  
    void Awake(){
        GetComponent<Interactable>().objectName = metalType.ToString();
    }
}
